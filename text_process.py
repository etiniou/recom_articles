import regex

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer 
import pandas as pd

stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
    
def clean_article_fields(dict_):
    """
    Replace None elements with "" for each article field
    """
    dict_clean = {}
        
    for key in ['abstract','title','createdAt', 'doi']:
        if dict_[key] is None:
            dict_clean[key] = ""
        else:
            dict_clean[key] = dict_[key]
    
    # Keyword field is in a list format -- change to a string by concatenating elements
    if dict_['keywords'] is None or None in dict_['keywords']:
        dict_clean['keywords'] = ""
    else:
        #dict_clean['keywords'] = dict_['keywords']
        dict_clean['keywords'] = " ".join(dict_['keywords'])
    
    return pd.Series(dict_clean)

    
def tokenize(sentences, min_length=1, max_length=30):
    """
    Cleans sentences by removing special characters, lemmatizing and convert to tokens.
    
    ---------
    Parameters:
        - sentences: list of sentences
        - min_length: minimum length of each word to be valid and added to corpus
        - max_length: maximum length "-"
        
    Output: 
        - tokens_out: clean list of tokens for each sentence 
    """
     
    tokens_out = []
    
    for sent in sentences:
    
        sent = regex.sub('[^\P{P}-]+',' ',sent)
#        sent = regex.sub(r'[0-9]+',' ',sent)
        
        sent_out = []
        for token in word_tokenize(sent.lower()):
            if token not in stop_words and len(token) > min_length and len(token) < max_length:
                
                token_processed = lemmatizer.lemmatize(token)
    
                sent_out.append(token_processed)

        tokens_out.append(sent_out)
        
    return tokens_out



    

    
