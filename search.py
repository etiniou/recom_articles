# Path names
data_dir = 'data/'
corpus_dir = 'corpuses/'
models_dir = 'models/'


import text_process

import pandas as pd
import numpy as np

import gensim.similarities as similarities
import gensim.matutils as matutils
from gensim.models import Word2Vec, TfidfModel
from gensim.models import Phrases
from gensim.models.phrases import Phraser
from gensim.corpora import Dictionary

import pickle

#df_articles_train = pd.read_pickle(data_dir+"./arxiv10k_train.pkl")


def choose_query(method, article_index, user_defined_query_dict):

    """
    Parameters:
        - method: method used for selecting query article:
            a) 'rand_test': picks a random article from test set
            b) 'fixed_test': picks a specific article from test set
            c) 'fixed_train': picks a specific article from train set
            d) 'used_defined': user enters query in a dictionary
        - article_index: index of article selected to use as query for methods
        b, c. Omit if methods a, d selected.
        - user_defined_query_dict: dictionary with keywords, title, abstract 
        info of user defined query. Omit if methods a-c selected.

    Output:
        Query sentence (non-tokenized)
    
    """
    
    query_cols = ['abstract','title','keywords']
    
    if method == 'rand_test':
        df_articles_test = pd.read_pickle(data_dir+"./arxiv10k_test.pkl")
        df_articles_test = df_articles_test.apply(text_process.clean_article_fields, axis=1)
        query = df_articles_test.sample(1)#, random_state=41)
        query= query.loc[:,query_cols].iloc[0]
        
    elif method == 'fixed_test':
        df_articles_test = pd.read_pickle(data_dir+"./arxiv10k_test.pkl")
        df_articles_test = df_articles_test.apply(text_process.clean_article_fields, axis=1)
        query = df_articles_test.sample(1)#), random_state=41)
        query = pd.read_pickle(data_dir+"./arxiv10k_test.pkl").iloc[article_index,:]
        query= query.loc[query_cols]
        
        
    elif method == 'fixed_train':
        
        query = pd.read_pickle(data_dir+"./arxiv10k_train.pkl").iloc[article_index,:]
        query= query.loc[query_cols]
        
    elif method == 'user_defined':
        
        query = pd.Series(user_defined_query_dict).to_frame().T
        query = query.apply(text_process.clean_article_fields, axis=1).iloc[0]
        query= query.loc[query_cols]
        
    else:
        
        print("Method used for selecting query article not recognised")
        query=None
        
        
    return query


def prepare_query_vec(model_types_dict, field, query):
    
    if model_types_dict[field] == 'w2v':
        
        query_vec = prepare_query_vec_w2v(query, field + '_w2v', field)
    
    elif model_types_dict[field] == 'tfidf':
        
        #    Load bi-gram dictionary
        dict_bigram = Dictionary.load_from_text(models_dir + field + '_' + model_types_dict[field] + '_bigram_dict')
        
        #    Load bi-gram phraser
        with open(models_dir + field + '_' + model_types_dict[field] + '_phraser.txt', "rb") as fp:   
        
            phraser = pickle.load(fp) 
              
        query_vec = prepare_query_vec_tfidf(query, field + '_tfidf', field, dict_bigram, phraser)
        
    return query_vec
        
        
def prepare_query_vec_w2v(query, model_name, field):
    
    model = Word2Vec.load(models_dir + model_name)
    
    query_tokenized = text_process.tokenize([query[field]])[0]
    

    query_vec = matutils.unitvec(sum((model[word] for word in query_tokenized if word in model), np.zeros(model.vector_size)))

    return query_vec



def prepare_query_vec_tfidf(query, model_name, field, dictionary, phraser):
    
    model = TfidfModel.load(models_dir + model_name)
    
    query_tokenized = text_process.tokenize([query[field]])[0]
    
    tokens = [phraser[query_tokenized]]
    
    query_vec_bow = [dictionary.doc2bow(text) for text in tokens]
    
    query_vec = model[query_vec_bow]
    
    return query_vec
    


def generate_vector_sums(corpus_tokenized, word2vec_model):
    
    """
    
    Converts a corpus of tokenized sentences to vectors by summing the word
    vectors obtained from a word2vec model
    
    ---------
    
    Parameters:
        - corpus_tokenized

    Output:
        - normalised document vector
    
    """
    
    for doc in corpus_tokenized:  
        yield matutils.unitvec(  
            sum(
                (word2vec_model[word] for word in doc if word in word2vec_model),
                np.zeros(word2vec_model.vector_size)
            )
        )

def index_for_similarity(field, model_types_dict):

    if model_types_dict[field] == 'w2v':
        
       index_for_sim =  index_for_similarity_w2v(field, model_types_dict)
       
    elif model_types_dict[field] == 'tfidf':
         
        index_for_sim =  index_for_similarity_tfidf(field, model_types_dict)
        
    #    Pickle indexed similarity
    with open(models_dir + field + '_' + model_types_dict[field] + '_indexed_sim', "wb") as fp:   

        pickle.dump(index_for_sim, fp)
        
    print('Indexed similarity for %s using %s stored' % (field,model_types_dict[field]))
    
    return index_for_sim
        
        
def index_for_similarity_w2v(field, model_types_dict):
    
    #     Load model
    model = Word2Vec.load(models_dir + field + '_' + model_types_dict[field])
    
    #    Load tokenized corpus
    with open(models_dir + field + '_' + model_types_dict[field] + '_corpus_token.txt', "rb") as fp:   

        corpus_tokenized = pickle.load(fp) 
          

    index_for_sim = similarities.Similarity(models_dir + 'index_similarity_'+model_types_dict[field],\
                                        generate_vector_sums(corpus_tokenized, 
                                                                model),
                                           num_features=model.vector_size)
                                        
    return index_for_sim

    
def index_for_similarity_tfidf(field, model_types_dict):

    #     Load model        
    model = TfidfModel.load(models_dir + field + '_' + model_types_dict[field])
    
    #    Load bi-gram dictionary
    dict_bigram = Dictionary.load_from_text(models_dir + field + '_' + model_types_dict[field] + '_bigram_dict')
    
    #    Load tokenized corpus
    with open(models_dir + field + '_' + model_types_dict[field] + '_corpus_token.txt', "rb") as fp:   

        corpus_tokenized = pickle.load(fp) 
    
    corpus_tfidf = model[[dict_bigram.doc2bow(text) for text in corpus_tokenized]]
        
    index_for_sim = similarities.\
    SparseMatrixSimilarity(corpus_tfidf,num_features=len(dict_bigram))
                                        
    return index_for_sim


    
def find_nearest_neighbours(query_vec, index_for_sim, field, model_types_dict, dataset):
    
    if model_types_dict[field] == 'w2v':

        sims = list(enumerate(index_for_sim[query_vec]))
        df_sims= pd.DataFrame(sims, columns=['article_index','sim_score'])
        
    elif model_types_dict[field] == 'tfidf':
        
        sims = index_for_sim[query_vec[0]]
        df_sims = pd.DataFrame(sims, columns=['sim_score'])
        df_sims['article_index']=df_sims.index

    df_sims['field_scored']=df_sims['article_index'].\
    apply(lambda x: dataset.iloc[x,:][field])

    df_sims =df_sims.sort_values(by='sim_score', ascending=False).\
    reset_index(drop=True)
    
    return df_sims


def weighted_similarity(weights_dict, df_abs, df_tit, df_key, dataset):
    
    df_tit = df_tit.sort_values(by='article_index', ascending=True).\
    reset_index(drop=True).loc[:,['article_index','sim_score']]
    
    df_abs = df_abs.sort_values(by='article_index', ascending=True).\
    reset_index(drop=True).loc[:,['article_index','sim_score']]
    
    df_key = df_key.sort_values(by='article_index', ascending=True).\
    reset_index(drop=True).loc[:,['article_index','sim_score']]
    
    df_scores_combined = ((weights_dict['title']*df_tit.loc[:,'sim_score']+\
    weights_dict['abstract']*df_abs.loc[:,'sim_score']+\
    weights_dict['keywords']*df_key.loc[:,'sim_score'])).to_frame()
                              
    df_scores_combined['article_index']=df_scores_combined.index
    
    df_scores_combined['title']=df_scores_combined['article_index'].\
    apply((lambda x: dataset.iloc[x,:]['title']
                     ))
    
    df_scores_combined['title_score']=df_scores_combined['article_index'].\
    apply((lambda x: (df_tit[df_tit['article_index']==x]['sim_score'].values[0])
                     ))    

    df_scores_combined['abstract']=df_scores_combined['article_index'].\
    apply((lambda x: dataset.iloc[x,:]['abstract']
                     ))
    
    df_scores_combined['abstract_score']=df_scores_combined['article_index'].\
    apply((lambda x: (df_abs[df_abs['article_index']==x]['sim_score'].values[0])
                     ))

    df_scores_combined['keywords']=df_scores_combined['article_index'].\
    apply((lambda x: dataset.iloc[x,:]['keywords']
                     ))
    
    df_scores_combined['keywords_score']=df_scores_combined['article_index'].\
    apply((lambda x: (df_key[df_key['article_index']==x]['sim_score'].values[0])
                     ))

    df_scores_combined =df_scores_combined.sort_values(by='sim_score', ascending=False).\
    reset_index(drop=True)
                          
    
    return df_scores_combined



        
