import search
import config
import pandas as pd 
import text_process
import pickle

data_dir = 'data/'
models_dir = 'models/'

top_rec = 50 # Top recommendations for output


model_types_dict = config.model_types_dict
weights_dict = config.weights_dict

df_articles_train = pd.read_pickle(data_dir+"arxiv10k_train.pkl")


#%% Select query article & clean fields
query = search.choose_query('rand_test', None, None)
print('Query selected: %s' % query)

#%%
# Given the tokenized corpus (for a given field), a word embedding model,
# index all embedded articles to use for similarity queries
#index_for_sim_title = search.index_for_similarity('title', model_types_dict)
#index_for_sim_keywords = search.index_for_similarity('keywords', model_types_dict)
#index_for_sim_abstract = search.index_for_similarity('abstract', model_types_dict)


for field in ['title','keywords','abstract']:
    
    #    Try loading indexed similarity
    try:
        
        #    Load indexed similarity
        with open(models_dir + field + '_' + model_types_dict[field] + '_indexed_sim', "rb") as fp:   
        
            exec("index_for_sim_%s= pickle.load(fp)" % field)
            
        print('Loaded index for similarity of %s using %s' % (field,model_types_dict[field] ))
        print('----')
     
    # If not found, run function
    
    except:
        
        print('Index for similarity of %s using %s not found: Creating index for similarity now...' % (field,model_types_dict[field] ))
        exec("index_for_sim_%s = search.index_for_similarity('%s', model_types_dict)" % (field,field))
        print('----')

            

#%% Prepare query article

query_vec_title = search.prepare_query_vec(model_types_dict, 'title', query)
query_vec_keywords = search.prepare_query_vec(model_types_dict, 'keywords', query)
query_vec_abstract = search.prepare_query_vec(model_types_dict, 'abstract', query)

#%% Find similarities of query article against indexed corpus
df_sim_articles_title = search.find_nearest_neighbours(query_vec_title, index_for_sim_title, 'title', model_types_dict, df_articles_train)
df_sim_articles_keywords = search.find_nearest_neighbours(query_vec_keywords, index_for_sim_keywords, 'keywords', model_types_dict, df_articles_train)
df_sim_articles_abstract = search.find_nearest_neighbours(query_vec_abstract, index_for_sim_abstract, 'abstract', model_types_dict, df_articles_train)


df_similarity_final_topN =search.weighted_similarity(weights_dict,\
                                         df_sim_articles_abstract,\
                                           df_sim_articles_title, \
                                           df_sim_articles_keywords, df_articles_train)

query_data = []
query_data.insert(0,query)
df_out=pd.concat([pd.DataFrame(query_data), df_similarity_final_topN], ignore_index=True, sort=True)
df_out = df_out.head(top_rec)
df_out.fillna(1,inplace=True)
df_out.loc[0,'article_index']=0

df_out.to_csv("recom_output.csv")
print("Top",str(top_rec),"recommendations stored into recom_output.csv file")