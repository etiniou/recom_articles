import text_process

from gensim.models import Word2Vec
from multiprocessing import cpu_count
from gensim import corpora
from gensim.models import TfidfModel
from gensim.models import Phrases
from gensim.models.phrases import Phraser
import pickle

# Path names
data_dir = 'data/'
models_dir = 'models/'


def train_model(dataset, field, model_types_dict, w2v_params):
    
    if model_types_dict[field] == 'w2v':
        
        
        
        # Train word2vec model with specified parameters
        trained_model, corpus_tokenized = train_word2vec(dataset[field].values.tolist(),
                                      w_size=w2v_params[field]['w_size'],
                                      w_window=w2v_params[field]['w_window'])
        
                        
        dict_bigram = None
        bigram_phraser = None 
        
    
    elif model_types_dict[field] == 'tfidf':
        
        trained_model,  corpus_tokenized, dict_bigram, bigram_phraser = train_tfidf(dataset[field].values.tolist())
    
        #   Save dictionary    
        dict_bigram.save_as_text(models_dir + field + '_' + model_types_dict[field] + '_bigram_dict')
        print('Bigram dictionary saved')
    
        #    Pickle phraser
        with open(models_dir + field + '_' + model_types_dict[field] + '_phraser.txt', "wb") as fp:   
    
            pickle.dump(bigram_phraser, fp)
            print('Bigram phraser saved')
                  
    
    #    Save trained model
    trained_model.save(models_dir + field + '_' + model_types_dict[field])
    print('Trained %s model saved' % model_types_dict[field] )
    
    #    Pickle tokenized corpus
    with open(models_dir + field + '_' + model_types_dict[field] + '_corpus_token.txt', "wb") as fp:   

        pickle.dump(corpus_tokenized, fp)
        print('Corpus tokenized saved')
        print('--------')
          
        
#    return trained_model, corpus_tokenized, dict_bigram, bigram_phraser 
    


def train_word2vec(sentences, w_size=200, w_window=5, w_min_count=5):
    """
    Parameters:
        - sentences: list of sentences
        - w_size: 
        - w_window:
        - w_wmin_count: 
    
    Output:
        Trained word2vec model
    
    """
    
    sentences_tokenized = text_process.tokenize(sentences)
    
    model = Word2Vec(sentences_tokenized, size=w_size, window=w_window, min_count=w_min_count, workers=cpu_count()) 
    #vocab = list(model.wv.vocab.keys())    
    
    return model, sentences_tokenized




def train_tfidf(sentences, t_threshold=1, t_irs='nfc'):
    """
    Parameters:
        - sentences: list of sentences
        - t_threshold: The minimum score for a bigram to be taken into account  
        - t_irs: Combination of weights for tf-idf (https://radimrehurek.com/gensim/models/tfidfmodel.html)
    
    Output:
        Trained tfidf model
    
    """

    sentences_tokenized = text_process.tokenize(sentences)
    
    bigram = Phrases(sentences_tokenized, min_count=1, threshold=t_threshold, delimiter=b' ')
    bigram_phraser = Phraser(bigram)    
    bi_tokens = bigram_phraser[sentences_tokenized]
   
    dictionary = corpora.Dictionary(bi_tokens)
    corpus = [dictionary.doc2bow(text) for text in bi_tokens]
    model = TfidfModel(corpus, smartirs = t_irs)  # fit model
    
    return model, bi_tokens, dictionary, bigram_phraser
    
    
#def load_apply_pretrained_glove(sentences):
#    """
#    Parameters:
#        - sentences: list of sentences
#        - w_size: 
#        - w_window:
#        - w_wmin_count: 
#    
#    Output:
#        Trained word2vec model
#    
#    """
#    
#    sentences_tokenized = text_process.tokenize(sentences)
#    
#    model = glove_model300(sentences_tokenized, size=w_size, window=w_window, min_count=w_min_count, workers=cpu_count()) 
#    #vocab = list(model.wv.vocab.keys())  
#    
#    #    Save trained model
#    glove_model300.save(models_dir + 'glove_model300')
#    print('Trained %s model saved' % model_types_dict[field] )
#    
#    from gensim.test.utils import datapath, get_tmpfile
#    from gensim.models import KeyedVectors
#    from gensim.scripts.glove2word2vec import glove2word2vec
#    
#    glove_file = models_dir + 'glove_model300'
#    tmp_file = get_tmpfile("test_word2vec.txt")
#    _ = glove2word2vec(glove_file, tmp_file)
#    model_TEST = KeyedVectors.load_word2vec_format(tmp_file)
#        
#    return model, sentences_tokenized        
    