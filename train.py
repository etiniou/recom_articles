import config
import json 
import pandas as pd
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')

# Path names
data_dir = 'data/'
models_dir = 'models/'


#%% Read data, split to train and test
n_test = 350 # For splitting train-test
 
import text_process
articles_train = []
articles_test = []
for id, line in enumerate(open(data_dir+'arxiv10k.txt', 'r')):
#    Randomly pick articles for testing
    if id%n_test == 0:
        articles_test.append(json.loads(line))
    else:
        articles_train.append(json.loads(line))


# Add training articles to dataset and clean all columns
df_articles_train = pd.DataFrame(articles_train)
df_articles_train = df_articles_train.apply(text_process.clean_article_fields, axis=1)
df_articles_train.to_pickle(data_dir+"arxiv10k_train.pkl")


# Add testing articles to dataset
df_articles_test = pd.DataFrame(articles_test)
df_articles_test.to_pickle(data_dir+"arxiv10k_test.pkl")



#%% Tokenize sentences & train word embedding models for each article field

import model
print("Will now train the selected embedings on abstracts, title and keywords")

# -----ABSTRACTS-----
model.train_model(df_articles_train, 'abstract', config.model_types_dict, config.w2v_params)
# -----TITLES-----
model.train_model(df_articles_train, 'title', config.model_types_dict, config.w2v_params)
# -----KEYWORDS-----
model.train_model(df_articles_train, 'keywords', config.model_types_dict, config.w2v_params)


#%% Exploratory analysis


df_articles_all = pd.concat([df_articles_test.apply(text_process.clean_article_fields, axis=1),
                             df_articles_train], axis=0)

from collections import Counter
import matplotlib.pyplot as plt




def make_flat_and_count(list_of_list_words):
    words_flat = [item for sublist in list_of_list_words for item in sublist]
    words_count = list(Counter(words_flat).most_common())
    return words_count


def plot_processed_terms_distribution(field, dataset, top_terms):
    
    sentences = dataset[field].values.tolist()
    sentences_tokenized = text_process.tokenize(sentences)
    
    # Find most popular words before pre-processing
    terms_count = make_flat_and_count(sentences_tokenized)

    df_count = pd.DataFrame(terms_count, columns=['token','frequency']).\
    iloc[0:top_terms,:]
    
    plt.figure()
    plt.bar(df_count['token'],df_count['frequency'])
    plt.xticks(rotation='vertical')
    plt.title('Top-%s terms in %s' % (str(top_terms), str(field)))
    plt.xlabel('Term')
    plt.ylabel('Frequency')
    #plt.savefig('graphs/term_freq_%s.png' %field)
    
    return len(terms_count)
    
#    return df_count
for field in ['keywords','title','abstract'] :
    n_terms =plot_processed_terms_distribution(field, df_articles_all, 20)   
    print('No of distinct terms in %s = %d' % (field, n_terms)) 
        
