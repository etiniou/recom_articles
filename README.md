# Atypon content-based article recommendation system #

####    How to run
* First clone the repo and cd into the main folder

```
git clone https://etiniou@bitbucket.org/etiniou/recom_articles.git
cd recom_articles
```

* **Optionally** you can create a virtualenv for Python to install all dependencies 

```
pip install virtualenv
virtualenv venv
source venv/bin/activate
```

* Pip install all dependencies

```
pip install regex nltk gensim pandas matplotlib
```

Split data to train and test and train model:

```
python train.py
```
This stores the trained w2v models and indexed vectors used to estimated similarity scoring in 'models' folder.

* Query a random article from test set and find most similar articles from train set:

```
python test.py
```
Recommendations for the query article are stored in 'recom_output.csv' file (comma delimited). First row corresponds to the query article. The first time that test will be executed will take slightly longer to run because index for the given set of documents is being built and stored for each text field and model.

