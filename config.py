model_types_dict={}
model_types_dict['abstract']= 'w2v'
model_types_dict['title']= 'w2v'
model_types_dict['keywords']= 'w2v' # tf-idf similarity distribution too skewed  

# Parameters used for word2vec models (when selected)
w2v_params={}
w2v_params['abstract']={'w_size': 200, 'w_window': 10}
w2v_params['title']={'w_size': 100, 'w_window': 5}
w2v_params['keywords']={'w_size': 10, 'w_window': 5}


# Weights used for calculating weighted similarity of all fields
weights_dict ={}
weights_dict['abstract']=0.2
weights_dict['title']=0.4
weights_dict['keywords']=0.4

# Parameter for top-N similar articles to extract
top_n = 10